import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  questions = [
    {id: 1, question: "¿Te atreves acuestionarla?", category: "Espiritual"},
    {id: 2, question: "¿Buscas guía espiritual a la hora de tomar decisiones?", category: "Espiritual"},
    {id: 3, question: "¿Te gustó nuestro test de cocina? ", category: "Cocina"},
    {id: 4, question: "¿Qué fruta seca sale de las uvas? ", category: "Cocina"},
    {id: 5, question: "¿Qué trabajo sería ese?", category: "General"},
    {id: 6, question: "¿Qué es lo último que has fotografiado en tu móvil? ", category: "General"},
    {id: 7, question: "¿Qué tienes en la nevera ahora mismo? ", category: "General"},
    {id: 8, question: "¿Si pudieras “des-inventar” alguna cosa, cuál sería? ", category: "General"},
    {id: 9, question: "¿Cómo te ves dentro de 5 años?", category: "General"},
    {id: 10, question: "¿Cuál es la manía que no quieres corregir de tu personalidad? ", category: "General"},

];

  filterQuestions='';

  constructor() { }

  ngOnInit() {
   console.log(this.getDimensionsByFilter(12)) 
  }

  getDimensionsByFilter(id){
    return this.questions.filter(x => x.id === id);
  }

}
