import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  forms: FormGroup;
  countCi=0;

  constructor(private fb: FormBuilder) { 
    this.forms = this.fb.group(
      {
        name: ['', [Validators.required,Validators.minLength(3),]],
        lastName: ['', [Validators.required]],
      }
    )
  }

  ngOnInit() {
  }

  count(){
    let count= Number(this.forms.controls.name.value.length)+(this.forms.controls.lastName.value.length)
   alert('El total de carteres es de '+count)
  }

}
