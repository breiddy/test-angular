import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms'

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  products=[
    {name:"Balón futbol", code:"00000", description:"Balón mundial", price:"50"},
    {name:"Pelota Beisbol", code:"051546", description:"Pelota de beisbol", price:"1"},
    {name:"Juego de mesa", code:"546547", description:"Juego de mesa multijugador", price:"15"},
    {name:"Lampara", code:"546547", description:"Lampara led", price:"10"},
  ]

  constructor() { }

  ngOnInit() {
  }

  delete(i) {
    this.products.splice(i, 1);
  }

  

}
