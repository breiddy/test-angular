import { Component, OnInit } from '@angular/core';
import { CartService } from './../../services/cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  products:any;
  total=0;
  
  constructor(public cartService:CartService) { }

  ngOnInit() {
    this.products= this.cartService.getCart();
    for (let i = 0; i < this.products.length; i++) {
      let mult = this.products[i].price*this.products[i].amount;
      this.total= this.total+mult
      mult=0;
    }
  }

  delete(i){
   if(confirm('Esta seguto de eliminar producto')){
    this.cartService.deleteCart(i)
   }
  }

}
