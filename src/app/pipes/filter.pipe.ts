import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(value: any, arg: any): any {
    const resultQuestions = [];
    for (const question of value) {
      if (question.question.toLowerCase().indexOf(arg.toLowerCase()) > -1) {
        resultQuestions.push(question)
      }
    }
    return resultQuestions;
  }

}
